package com.pmc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static com.pmc.SatelliteViolation.Type.*;
import static com.pmc.SatelliteComponent.*;

/**
 * These tests verify the logic in {@link SatelliteMonitor}
 * For each test, we process a set of telemetry data, 1 data point at a time
 * To keep things simple, test setup only deals with the properties that actually
 * matter to {@link SatelliteMonitor} which means we don't consider the satellite id or
 * yellow thresholds, and we always use the same red thresholds; we simply modify
 * the raw value to reflect the desired violation type (high, low, or none)
 * See {@link SatelliteMonitorTest#verifyMonitorProcessing} for more information
 */
public class SatelliteMonitorTest {

    @BeforeEach
    public void setupTest() {
        monitor = new SatelliteMonitor();
    }

    /**
     * Generate enough violations (3) for a battery alert and a tstat alert
     */
    @Test
    public void GenerateAlerts() {
        verifyMonitorProcessing(BATT, -1, timeOffset(0), null);
        verifyMonitorProcessing(BATT, -1, timeOffset(1), null);
        verifyMonitorProcessing(TSTAT, 101, timeOffset(2), null);
        verifyMonitorProcessing(TSTAT, 101, timeOffset(3), null);

        // 3rd tstat and battery violations should generate alerts
        verifyMonitorProcessing(BATT, -1, timeOffset(4), timeOffset(0));
        verifyMonitorProcessing(TSTAT, 101, timeOffset(5), timeOffset(2));
    }


    /**
     * Low tstat values and high battery values should not generate violations/alerts
     */
    @Test
    public void DoGenerateAlertsForTheOppositeExtreme() {
        verifyMonitorProcessing(BATT, 101, timeOffset(0), null);
        verifyMonitorProcessing(BATT, 101, timeOffset(1), null);
        verifyMonitorProcessing(BATT, 101, timeOffset(2), null);
        verifyMonitorProcessing(TSTAT, -1, timeOffset(3), null);
        verifyMonitorProcessing(TSTAT, -1, timeOffset(4), null);
        verifyMonitorProcessing(TSTAT, -1, timeOffset(5), null);
    }

    /**
     * If there are enough violations within the allowed interval, generate an
     * alert, if the interval has elapsed from the initial timestamp, even
     * if the first data point is outside the interval
     */
    @Test
    public void GenerateAlertIfEnoughViolationsWithinInterval() {
        verifyMonitorProcessing(BATT, -1, timeOffset(0), null);
        verifyMonitorProcessing(BATT, -1, timeOffset(4), null);
        verifyMonitorProcessing(BATT, -1, timeOffset(6), null);
        verifyMonitorProcessing(BATT, -1, timeOffset(7), timeOffset(4));
    }

    /**
     * Normal readings should not, in and of themselves, prevent an alert. If there are
     * enough violations within the interval, generate an alert, even if there are some normal
     * readings in between
     */
    @Test
    public void GenerateAlertEvenWithSomeNormalReadings() {
        verifyMonitorProcessing(BATT, -1, timeOffset(0), null);
        verifyMonitorProcessing(BATT, 50, timeOffset(1), null);
        verifyMonitorProcessing(BATT, -1, timeOffset(2), null);
        verifyMonitorProcessing(BATT, -1, timeOffset(3), timeOffset(0));
    }

    /**
     * If there are enough violations to generate an alert, but are not all within
     * the specified interval (5 seconds), do not generate an alert
     */
    @Test
    public void DoNotGenerateAlertIfIntervalExceeded() {
        verifyMonitorProcessing(BATT, -1, timeOffset(0), null);
        verifyMonitorProcessing(BATT, -1, timeOffset(3), null);
        verifyMonitorProcessing(BATT, -1, timeOffset(6), null);
    }

    /**
     * Once we are in alert, a single normal reading should clear the alert
     * (so it would take another 3 violations to generate another alert)
     */
    @Test
    public void NormalReadingClearsAlert() {
        verifyMonitorProcessing(BATT, -1, timeOffset(0), null);
        verifyMonitorProcessing(BATT, -1, timeOffset(0.5), null);
        verifyMonitorProcessing(BATT, -1, timeOffset(1), timeOffset(0));
        verifyMonitorProcessing(BATT, 50, timeOffset(1.5), null);
        verifyMonitorProcessing(BATT, -1, timeOffset(2), null);
        verifyMonitorProcessing(BATT, -1, timeOffset(3), null);
        verifyMonitorProcessing(BATT, -1, timeOffset(4), timeOffset(2));
    }

    /**
     * Creates a {@link SatelliteTelemetryData} point from the first 3 parameters, feeds
     * that data to the monitor, and verifies the result
     * @param expectedViolationTimestamp If null, we expect the monitor to return null; if not null,
     *                                   we expect the monitor to return a violation with a timestamp
     *                                   that matches this value
     */
    private void verifyMonitorProcessing(
            SatelliteComponent component, long value, Date timestamp, Date expectedViolationTimestamp) {

        SatelliteTelemetryData data =
                createTelemetryData(component, value, timestamp);
        SatelliteViolation result = monitor.process(data);
        if (expectedViolationTimestamp == null) {
            Assertions.assertNull(result);
        } else {
            Assertions.assertNotNull(result);
            Assertions.assertEquals(component, result.getComponent());
            Assertions.assertEquals(component == BATT ? LOW : HIGH, result.getType());
            Assertions.assertEquals(expectedViolationTimestamp, result.getTimestamp());
        }
    }

    /**
     * Create a {@link SatelliteTelemetryData} point with the specified data
     */
    private SatelliteTelemetryData createTelemetryData(SatelliteComponent component,
                                                       long value,
                                                       Date timestamp) {
        return new SatelliteTelemetryData(
                timestamp, "ID", 100, 90, 10, 5, value, component);
    }

    /**
     * Generate a time that's offset from the initial time by the specified number of minutes
     */
    private Date timeOffset(double minutes) {
        return new Date(initialTime.getTime() + (long) (minutes * 1000 * 60));
    }

    private SatelliteMonitor monitor;
    private final Date initialTime = new Date();
}
