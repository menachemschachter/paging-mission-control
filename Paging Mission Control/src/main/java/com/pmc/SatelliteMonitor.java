package com.pmc;

import java.util.*;

/**
 * A monitor that keeps track of the telemetry data for a specific
 * satellite and generates alerts when appropriate
 */
public class SatelliteMonitor {

    /**
     * Process a single telemetry data point
     * @param data - The data to process
     * @return IF the telemetry data generates an alert, returns
     * the relevant Violation information otherwise, returns null
     */
    public SatelliteViolation process(SatelliteTelemetryData data) {
        SatelliteViolation.Type violationType = getViolationType(data);

        Queue<SatelliteViolation> currentViolations =
                getCurrentViolations(data.getComponent());
        SatelliteViolation firstViolation = currentViolations.peek();

        // If we already generated an alert
        if (currentViolations.size() == ViolationsUntilAlert) {
            if (violationType == SatelliteViolation.Type.NONE) {
                // The new data is not in violation, so we should clear
                // the data for the current alert. Otherwise, we already
                // generated an alert, and this is another violation
                // of the same type, so we do nothing
                currentViolations.clear();
            }
            return null;
        }

        // Ignore non-violation data (unless it clears an alert, which was handled above)
        if (violationType == SatelliteViolation.Type.NONE) {
            return null;
        }

        // Remove any stale data from the queue
        while(isFirstViolationExpired(data)) {
            currentViolations.remove();
        }

        // Add the new violation
        currentViolations.add(
                new SatelliteViolation(data.getId(),
                        violationType,
                        data.getComponent(),
                        data.getTimestamp()));

        // If we've now reached the number of violations to generate an alert
        // return the first violation
        if (currentViolations.size() == ViolationsUntilAlert) {
            return firstViolation;
        }

        return null;
   }

    /**
     * Whether the first violation in the relevant list has expired
     * (i.e. whether {@value #MaximumIntervalUntilAlert} ms have elapsed
     */
    private boolean isFirstViolationExpired(SatelliteTelemetryData newData) {
        Queue<SatelliteViolation> currentViolations =
                getCurrentViolations(newData.getComponent());
        if (currentViolations.peek() == null) {
            return false;
        }

        Date timestamp = newData.getTimestamp();
        Date timestampOfFirstViolation = currentViolations.peek().getTimestamp();
        return timestamp.getTime() - timestampOfFirstViolation.getTime() >
                MaximumIntervalUntilAlert * 1000;
    }

    /**
     * Gets the Violation type for the specified telemetry data
     */
    private SatelliteViolation.Type getViolationType(SatelliteTelemetryData data) {
        if (data.getValue() < data.getRedLow() &&
                data.getComponent() == SatelliteComponent.BATT) {
            return SatelliteViolation.Type.LOW;
        } else if (data.getValue() > data.getRedHigh() &&
                data.getComponent() == SatelliteComponent.TSTAT) {
            return SatelliteViolation.Type.HIGH;
        } else {
            return SatelliteViolation.Type.NONE;
        }
    }

    /**
     * Gets the queue of current violations associated with the
     * specified component
     */
    private Queue<SatelliteViolation> getCurrentViolations(SatelliteComponent component) {
        if (component == SatelliteComponent.BATT) {
            return currentBatteryViolations;
        } else {
            return currentThermostatViolations;
        }
    }

    // Note: I could have combined these into a single data map of type
    // HashMap<SatelliteComponent, Queue<SatelliteViolation>>
    // and if there were more than 2 components, that's what I would probably do,
    // but since there are only 2 components, this seems less confusing
    private final Queue<SatelliteViolation> currentThermostatViolations = new LinkedList<>();
    private final Queue<SatelliteViolation> currentBatteryViolations = new LinkedList<>();

    // The number of violations (within the specified interval) at which an alert is generated
    private static final int ViolationsUntilAlert = 3;

    // The maximum time (in seconds) that a set of violations can span and still be considered
    // a single group of violations
    private static final long MaximumIntervalUntilAlert = 5 * 60; // 5 minutes
}
