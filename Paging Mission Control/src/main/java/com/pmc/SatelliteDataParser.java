package com.pmc;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Parses satellite telemetry data into {@link SatelliteTelemetryData}
 */
public class SatelliteDataParser {

    /**
     * Parse the contents of an entire file into a list of {@link SatelliteTelemetryData}
     */
    public List<SatelliteTelemetryData> parseFile(String filename) throws IOException, ParseException {
        List<SatelliteTelemetryData> dataset = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                dataset.add(parseLine(line));
            }
        }
        return dataset;
    }

    /**
     * Convert a single line of text to a {@link SatelliteTelemetryData} entity
     * Data will take the following form
     * <timestamp>|<satellite-id>|<red-high-limit>|<yellow-high-limit>|<yellow-low-limit>|<red-low-limit>|<raw-value>|<component>
     *
     * Example: 20180101 23:01:05.001|1001|101|98|25|20|99.9|TSTAT
     */
    public SatelliteTelemetryData parseLine(String input) throws ParseException {
        String[] data = input.split("\\|");
        Date timestamp = formatter.parse(data[0]);
        String id = data[1];
        double redHigh = Double.parseDouble(data[2]);
        double yellowHigh = Double.parseDouble(data[3]);
        double yellowLow = Double.parseDouble(data[4]);
        double redLow = Double.parseDouble(data[5]);
        double value = Double.parseDouble(data[6]);
        SatelliteComponent type = SatelliteComponent.valueOf(data[7]);
        return new SatelliteTelemetryData(timestamp, id, redHigh, yellowHigh, yellowLow, redLow, value, type);
    }

    private final DateFormat formatter = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
}
