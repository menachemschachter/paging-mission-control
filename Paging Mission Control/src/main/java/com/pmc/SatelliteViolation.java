package com.pmc;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 * Contains information about a specific telemetry data point that was in violation,
 * i.e. either the value was higher than red-high or lower than red-low
 */
@JsonPropertyOrder({"satelliteId", "severity","component", "timestamp" })
public class SatelliteViolation {

    public SatelliteViolation(String satelliteId, Type type, SatelliteComponent component, Date timestamp) {
        this.satelliteId = satelliteId;
        this.type = type;
        this.component = component;
        this.timestamp = timestamp;
    }

    /**
     * The satellite id
     */
    public String getSatelliteId() {
        return satelliteId;
    }

    /**
     * The satellite id
     */
    public void setSatelliteId(String satelliteId) {
        this.satelliteId = satelliteId;
    }

    /**
     * The violation type
     * (The json property name is "severity" as per the project specifications,
     *  but that name doesn't really make sense, so internally I called the
     *  property "Type")
     */
    @JsonProperty("severity")
    public Type getType() {
        return type;
    }

    /**
     * The violation type
     */
    public void setType(Type type) {
        this.type = type;
    }

    /**
     * The satellite component associated with this violation
     */
    public SatelliteComponent getComponent() {
        return component;
    }

    /**
     * The satellite component associated with this violation
     */
    public void setComponent(SatelliteComponent component) {
        this.component = component;
    }

    /**
     * The timestamp associated with this violation
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * Returns the timestamp as a {@link ZonedDateTime} for json serialization
     * as per project specifications
     */
    @JsonProperty("timestamp")
    public ZonedDateTime getTimestampUtc() {
        return ZonedDateTime.of(LocalDateTime.ofInstant(timestamp.toInstant(),
                ZoneId.systemDefault()), ZoneId.of("GMT"));
    }

    /**
     * The timestamp associated with this violation
     */
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "SatelliteViolation{" +
                "satelliteId='" + satelliteId + '\'' +
                ", severity=" + type +
                ", component=" + component +
                ", timestamp=" + timestamp +
                '}';
    }

    private String satelliteId;
    private Type type;
    private SatelliteComponent component;
    private Date timestamp;

    /**
     * The type of violation
     */
    public enum Type {
        /**
         * No violation
         */
        NONE("NO ALERT"),

        /**
         * The telemetry value was higher than the red-high value
         */
        HIGH("RED HIGH"),

        /**
         * The telemetry value was lower than the red-low value
         */
        LOW("RED LOW");

        private final String mText;
        Type(String text) {
            mText = text;
        }

        @Override
        public String toString() {
            return mText;
        }
    }
}