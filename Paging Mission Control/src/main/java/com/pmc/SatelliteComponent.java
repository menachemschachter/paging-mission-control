package com.pmc;

/**
 * A satellite component
 */
public enum SatelliteComponent {
    /**
     * The battery
     */
    BATT,

    /**
     * The thermostat
     */
    TSTAT
}
