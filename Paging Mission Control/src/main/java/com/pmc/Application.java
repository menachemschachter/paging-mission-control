package com.pmc;

import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * The entry point for the application
 */
public class Application {

    /**
     * Application entry point
     */
    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("Argument expected: file name to parse");
            return;
        }

        try {
            // Parse the telemetry data from the file
            List<SatelliteTelemetryData> dataset = new SatelliteDataParser()
                    .parseFile(args[0]);

            // Just in case the data is not in sequential order
            dataset.sort(Comparator.comparing(SatelliteTelemetryData::getTimestamp));

            // Send the telemetry data points to monitors for processing and
            // collect any alerts generated
            List<SatelliteViolation> alerts = new ArrayList<>();
            for (SatelliteTelemetryData data : dataset) {
                SatelliteMonitor monitor = monitors.get(data.getId());
                if (monitor == null) {
                    monitor = new SatelliteMonitor();
                    monitors.put(data.getId(), monitor);
                }
                SatelliteViolation alert = monitor.process(data);
                if (alert != null) {
                    alerts.add(alert);
                }
            }

            // Output the alert list as json. There's a lot of configuration
            // here. This was needed to exactly match the output format
            // shown in the requirements
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
            mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"));
            mapper.registerModule(new JavaTimeModule());

            DefaultPrettyPrinter.Indenter indenter =
                    new DefaultIndenter("    ", DefaultIndenter.SYS_LF);
            DefaultPrettyPrinter printer = new DefaultPrettyPrinter();
            printer.indentObjectsWith(indenter);
            printer.indentArraysWith(indenter);

            String json = mapper.writer(printer).writeValueAsString(alerts);
            System.out.println(json);

        } catch (Exception ex) {
            // Really the only exceptions we would expect are parsing issues
            // and the requirements state we can omit error handling for that
            // so any errors would be pretty unexpected, and there's not much
            // more that we can do other than let the user know
            System.out.println("Unexpected error: " + ex);
            ex.printStackTrace();
        }
    }

    // A map of satellite monitors, indexed by satellite id
    private static final HashMap<String, SatelliteMonitor> monitors = new HashMap<>();
}
