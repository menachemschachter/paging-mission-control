package com.pmc;

import java.util.Date;

/**
 * Telemetry data for a satellite
 */
public class SatelliteTelemetryData {

    /**
     * Constructor that takes values for all properties
     */
    public SatelliteTelemetryData(Date timestamp, String id, double redHigh, double yellowHigh, double yellowLow, double redLow, double value, SatelliteComponent component) {
        this.timestamp = timestamp;
        this.id = id;
        this.redHigh = redHigh;
        this.yellowHigh = yellowHigh;
        this.yellowLow = yellowLow;
        this.redLow = redLow;
        this.value = value;
        this.component = component;
    }

    /**
     * The timestamp associated with the data
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * The timestamp associated with the data
     */
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * The satellite id for the satellite that the data refers to
     */
    public String getId() {
        return id;
    }

    /**
     * The satellite id for the satellite that the data refers to
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * If the actual measurement is higher than this value, consider it "red" severity
     */
    public double getRedHigh() {
        return redHigh;
    }

    /**
     * If the actual measurement is higher than this value, consider it "red" severity
     */
    public void setRedHigh(double redHigh) {
        this.redHigh = redHigh;
    }

    /**
     * If the actual measurement is lower than this value, consider it "red" severity
     */
    public double getRedLow() {
        return redLow;
    }

    /**
     * If the actual measurement is lower than this value, consider it "red" severity
     */
    public void setRedLow(double redLow) {
        this.redLow = redLow;
    }

    /**
     * If the actual measurement is higher than this value, consider it "yellow" severity
     */
    public double getYellowHigh() {
        return yellowHigh;
    }

    /**
     * If the actual measurement is higher than this value, consider it "yellow" severity
     */
    public void setYellowHigh(double yellowHigh) {
        this.yellowHigh = yellowHigh;
    }

    /**
     * If the actual measurement is lower than this value, consider it "yellow" severity
     */
    public double getYellowLow() {
        return yellowLow;
    }

    /**
     * If the actual measurement is lower than this value, consider it "yellow" severity
     */
    public void setYellowLow(double yellowLow) {
        this.yellowLow = yellowLow;
    }

    /**
     * The actual raw value of the measurement
     */
    public double getValue() {
        return value;
    }

    /**
     * The actual raw value of the measurement
     */
    public void setValue(double value) {
        this.value = value;
    }

    /**
     * The satellite component associated with this data
     */
    public SatelliteComponent getComponent() {
        return component;
    }

    /**
     * The satellite component associated with this data
     */
    public void setType(SatelliteComponent component) {
        this.component = component;
    }

    @Override
    public String toString() {
        return "SatelliteData{" +
                "timestamp=" + timestamp +
                ", id='" + id + '\'' +
                ", redHigh=" + redHigh +
                ", redLow=" + redLow +
                ", yellowHigh=" + yellowHigh +
                ", yellowLow=" + yellowLow +
                ", value=" + value +
                ", type=" + component +
                '}';
    }

    private Date timestamp;
    private String id;
    private double redHigh;
    private double redLow;
    private double yellowHigh;
    private double yellowLow;
    private double value;
    private SatelliteComponent component;
}
