Instructions for running the application:
This application was developed with IntelliJ 2021.2 and JDK 16. You can run it
in IntelliJ as well, or you can run it from the command line with the provided
jar file like this (substituting sample.txt with the name of your input file:
java -jar "Paging mission control.jar" sample.txt

3rd party attribution:
The only 3rd party library I used was the Jackson library for the json output
(https://github.com/FasterXML/jackson).

Application Description:
The project can be broken down into 3 parts. We need to 1) parse the telemetry
data from the input file, 2) process the data and generate any alerts, and then
3) convert the generated alerts to json and print them out to the console.

To parse the telemetry data, I use a SatelliteDataParser class, which reads
and parses the file and returns a list of SatelliteTelemetryData entities.

The application then feeds the data to a set of SatelliteMonitors (1 per
satellite) for processing - to generate any alerts. Each monitor maintains
its own internal state, so that it can use previous data, in addition to the new
incoming data point, to determine whether to generate an alert.

Finally, after the application has collected all the alerts, it converts them
to Json using the Jackson library and prints them to the console

Note:
I made 2 assumptions about points that didn't seem clear in the instructions.
I tried asking the hiring manager, and he agreed with my assumptions, but he
wasn't certain. These are the 2 assumptions:

1) If you have 2 violation data points, then 1 normal, and then 1 more violation,
all within the 5-minute interval, should that generate an alert? The instruction
is "If for the same satellite there are three battery voltage readings that are
under the red low limit within a five-minute interval." It doesn't say that they
need to be consecutive, so I assumed that they don't need to be consecutive.

2) If there are more than 3 violations, it seems like we don't continue
to generate alerts, but when do we consider the alert to be cleared? It doesn't
specify, so my assumption is that if we have any readings that aren't violations,
then we consider the alert to be cleared. Therefore, if afterwards, we have
another 3 consecutive violations, we would generate another alert.